import React from "react"
import ShopProvider from "./src/components/context/ShopProvider"

export const wrapRootElement = ({ element, props }) => {
  return <ShopProvider>{element}</ShopProvider>
}
