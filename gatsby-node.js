const path = require("path")
require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

const { createFilePath } = require("gatsby-source-filesystem")

exports.createPages = async ({ graphql, actions, reporter }) => {
  const pages = await graphql(`
    query {
      allStripeSku {
        edges {
          node {
            id
            product {
              id
              name
            }
          }
        }
      }
    }
  `)

  const { createPage } = actions

  pages.data.allStripeSku.edges.forEach(({ node }) => {
    let handle = node.product.name
      .toLowerCase()
      .split(" ")
      .join("-")
    createPage({
      path: `/product/${handle}`,
      component: path.resolve("./src/templates/productPageTemplate.js"),
      context: {
        handle: handle,
      },
    })
  })
}
