import React from "react"
import ShopProvider from "./src/components/context/ShopProvider"

export const onRenderBody = ({ setPostBodyComponents }) => {
  setPostBodyComponents([<script src="https://js.stripe.com/v3/"></script>])
}

export const wrapRootElement = ({ element, props }) => {
  if (element.key === "/shop/") {
    return <ShopProvider>{element}</ShopProvider>
  }
}
