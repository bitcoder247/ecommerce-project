import React from "react"
import { css } from "@emotion/core"

import { PolarBear } from "../../styles/business/businessStyles"
import {
  TextContainer,
  SVGContainer,
  SectionContainer,
  GeneralButton,
} from "../../styles/general/generalStyles"

const Business = ({contactScroll}) => {

  return (
    <SectionContainer
      css={css`
        @media screen and (min-width: 1200px) {
          flex-direction: row !important;
        }
      `}
    >
      <SVGContainer>
        <PolarBear />
      </SVGContainer>
      <TextContainer>
        <h2>You want our frozen desserts?</h2>
        <h2>We'll deliver to your business.</h2>
        <p>Have your customers happy with our desserts.</p>
        <GeneralButton onClick={() => contactScroll()}>Make An Order</GeneralButton>
      </TextContainer>
    </SectionContainer>
  )
}

export default Business
