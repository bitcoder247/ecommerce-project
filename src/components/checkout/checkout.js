import React, { useState, useEffect } from "react"

const Checkout = () => {
  const [state, setState] = useState({
    stripe: null,
  })

  const { stripe } = state

  const redirectToCheckout = async e => {
    e.preventDefault()
    let success = ""
    let cancel = ""

    if (stripe === null) return
    if (process.env.NODE_ENV === "development") {
      success = process.env.SUCCESS_URL_DEV
      cancel = process.env.CANCEL_URL_DEV
    } else {
      // The web urls for when app is built
    }
    const { error } = await stripe.redirectToCheckout({
      items: [{ sku: "sku_GM3yMRHYRVcJQI", quantity: 1 }],
      successUrl: success,
      cancelUrl: cancel,
    })
    console.warn("Error:", error)
  }

  useEffect(() => {
    setState(prevState => {
      return {
        ...prevState,
        stripe: window.Stripe("pk_test_XHsUvh94fvoQciGnk1OVmdh3007YMKPM9z"),
      }
    })
  }, [])

  return <button onClick={redirectToCheckout}>Check out</button>
}

export default Checkout
