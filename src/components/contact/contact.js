import React, { useState, forwardRef } from "react"
import { css } from "@emotion/core"

import { FormContainer, Form } from "../../styles/contact/contactStyles"
import {
  SectionContainer,
  TextContainer,
  GeneralButton,
} from "../../styles/general/generalStyles"

const Contact = (props, ref) => {
  const [state, setState] = useState({
    name: "",
    email: "",
    phone: "",
  })

  const {name, email, phone} = state

  const handleChange = e => {
    const {name, value} = e.target

    setState({
      ...state,
      [name]: value,
    })
  }


  return (
    <SectionContainer
      ref={ref}
      css={css`
        @media screen and (min-width: 1200px) {
          flex-direction: row;
        }
      `}
    >
      <TextContainer>
        <h2>Want to communicate with us?</h2>
        <h2>We'll get to you right away.</h2>
        <p>Fill out the form and we can start from there.</p>
      </TextContainer>
      <FormContainer>
        <Form>
          <label htmlFor="name">Full Name:</label>
          <input type="text" name="name" value={name} onChange={handleChange} />
          <label htmlFor="email">Email:</label>
          <input
            type="email"
            name="email"
            value={email}
            onChange={handleChange}
          />
          <label htmlFor="phone">Phone:</label>
          <input
            type="text"
            name="phone"
            value={phone}
            onChange={handleChange}
          />

          <div>
            <GeneralButton type="submit">Submit</GeneralButton>
          </div>
        </Form>
      </FormContainer>
    </SectionContainer>
  )
}

const ContactForward = forwardRef(Contact)

export default ContactForward
