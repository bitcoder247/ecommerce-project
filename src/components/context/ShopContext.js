import { createContext } from "react"

const defaultState = {
  products: [],
  name: "",
  display: false,
  totalItems: 0,
  getname: () => {},
  addProduct: () => {},
  addQuantity: () => {},
  removeProduct: () => {},
  clearCart: () => {}
}

const ShopContext = createContext(defaultState)

export default ShopContext
