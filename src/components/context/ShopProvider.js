import React, { useReducer } from "react"
import shopReducer from "./shopReducer"
import ShopContext from "./ShopContext"
import {
  ADD_PRODUCT,
  GET_NAME,
  ADD_QUANTITY,
  REMOVE_PRODUCT,
  CLEAR_CART,
} from "./types"

const ShopProvider = ({ children }) => {
  const [state, dispatch] = useReducer(shopReducer, {
    products: [],
    name: "",
    display: false,
    totalItems: 0,
  })

  const getName = name => {
    dispatch({
      type: GET_NAME,
      name: name,
    })
  }

  const addProduct = product => {
    if (state.products.length === 0) {
      dispatch({
        type: ADD_PRODUCT,
        product: { ...product },
      })
    } else if (
      state.products.some(elem => {
        return elem["sku_id"] === product["sku_id"]
      }) !== true
    ) {
      dispatch({
        type: ADD_PRODUCT,
        product: { ...product },
      })
    } else if (
      state.products.some(elem => {
        if (elem["sku_id"] === product["sku_id"] + "_" + product["size"]) {
          return true
        }
        return false
      }) === true
    ) {

      dispatch({
        type: ADD_QUANTITY,
        product: { ...product },
      })
    } else if (
      state.products.some(elem => {
        if (elem["sku_id"] === product["sku_id"]) {
          return elem["size"] === product["size"]
        }
        return false
      }) !== true
    ) {
      product["sku_id"] = product["sku_id"] + "_" + product["size"]
      dispatch({
        type: ADD_PRODUCT,
        product: { ...product },
      })
    } else {
      dispatch({
        type: ADD_QUANTITY,
        product: { ...product },
      })
    }
  }

  const addQuantity = (product, num) => {
    if (num == null) {
      dispatch({
        type: ADD_QUANTITY,
        product: { ...product },
      })
    } else {
      product.num = num

      dispatch({
        type: ADD_QUANTITY,
        product: { ...product },
      })
    }
  }

  const removeProduct = id => {
    dispatch({
      type: REMOVE_PRODUCT,
      id: id,
    })
  }

  const clearCart = () => {
    dispatch({
      type: CLEAR_CART,
    })
  }

  return (
    <ShopContext.Provider
      value={{
        products: state.products,
        name: state.name,
        display: state.display,
        totalItems: state.totalItems,
        getName: getName,
        addProduct: addProduct,
        addQuantity: addQuantity,
        removeProduct: removeProduct,
        clearCart: clearCart,
      }}
    >
      {children}
    </ShopContext.Provider>
  )
}

export default ShopProvider
