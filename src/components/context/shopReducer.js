import {
  ADD_PRODUCT,
  GET_NAME,
  ADD_QUANTITY,
  REMOVE_PRODUCT,
  CLEAR_CART,
} from "./types"

const shopReducer = (state, actions) => {
  switch (actions.type) {
    case ADD_PRODUCT:
      return {
        ...state,
        display: true,
        products: state.products.concat({ ...actions.product }),
        totalItems: state.totalItems + actions.product.quantity,
      }

    case GET_NAME:
      return {
        ...state,
        name: actions.name,
      }
    case ADD_QUANTITY:
      if (actions.product.num) {
        if (actions.product.num > actions.product.quantity) {
          return {
            ...state,
            products: state.products.map(elem => {
              if (elem["sku_id"] === actions.product["sku_id"]) {
                elem.quantity = actions.product.num
                return elem
              }
              return elem
            }),
            totalItems:
              state.totalItems +
              (actions.product.num - actions.product.quantity),
          }
        } else {
          return {
            ...state,
            products: state.products.map(elem => {
              if (elem["sku_id"] === actions.product["sku_id"]) {
                elem.quantity = actions.product.num
                return elem
              }
              return elem
            }),
            totalItems:
              state.totalItems -
              (actions.product.quantity - actions.product.num),
          }
        }
      } else {
        return {
          ...state,
          display: true,
          products: state.products.map(elem => {
            if (elem["sku_id"] === actions.product["sku_id"]) {
              elem.quantity =
                actions.product.quantity === 1
                  ? elem.quantity + 1
                  : elem.quantity + actions.product.quantity
              return elem
            } else if (
              elem["sku_id"] ===
              actions.product["sku_id"] + "_" + actions.product["size"]
            ) {
              elem.quantity =
                actions.product.quantity === 1
                  ? elem.quantity + 1
                  : elem.quantity + actions.product.quantity
              return elem
            } else {
              return elem
            }
          }),
          totalItems: state.totalItems + actions.product.quantity,
        }
      }
    case REMOVE_PRODUCT:
      const filteredArr = state.products.filter(elem => {
        if (elem["sku_id"] === actions.id) {
          return false
        } else {
          return true
        }
      })
      return {
        ...state,
        products: filteredArr,
        totalItems:
          filteredArr.length === 0
            ? 0
            : filteredArr.reduce((acc, curr) => {
                return acc + curr.quantity
              }, 0),
      }
    case CLEAR_CART:

      return {
        ...state,
        products: [],
        name: "",
        display: false,
        totalItems: 0,
      }
    default:
      return {
        ...state,
      }
  }
}

export default shopReducer
