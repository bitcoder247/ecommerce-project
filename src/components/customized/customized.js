import React from "react"

import { Swan } from "../../styles/customized/customizedStyles"

import {
  TextContainer,
  SVGContainer,
  SectionContainer,
  GeneralButton,
} from "../../styles/general/generalStyles"

const Customized = ({ contactScroll }) => {
  return (
    <SectionContainer>
      <SVGContainer>
        <Swan />
      </SVGContainer>
      <TextContainer>
        <h2>You need something custom</h2>
        <h2>For a special event?</h2>
        <p>
          We would like to make you something custom for whatever special event
          you have.
        </p>
        <GeneralButton onClick={() => contactScroll()}>
          Contact Us
        </GeneralButton>
      </TextContainer>
    </SectionContainer>
  )
}

export default Customized
