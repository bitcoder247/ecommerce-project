import React, { forwardRef } from "react"
import {
  SectionContainer,
  SVGContainer,
  GeneralButton,
  TextContainer,
} from "../../styles/general/generalStyles"
import { FamilySVG } from "../../styles/family/familyStyles"

const Family = (props, ref) => {
  return (
    <SectionContainer ref={ref}>
      <SVGContainer>
        <FamilySVG />
      </SVGContainer>
      <TextContainer>
        <h2>Come to our shop and</h2>
        <h2>make memorable moments.</h2>
        <p>
          Come alone, or bring your friends and family. We offer quality frozen
          desserts, you will have a great time.
        </p>
        <GeneralButton>Our Shop</GeneralButton>
      </TextContainer>
    </SectionContainer>
  )
}

const FamilyForward = forwardRef(Family)

export default FamilyForward
