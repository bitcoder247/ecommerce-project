import React from "react"

import {
  FooterContainer,
  FooterCopy,
  FooterIconWrapper,
} from "../../styles/footer/footerStyles"
import { FlexCenter } from "../../styles/general/generalStyles"

const Footer = () => {
  return (
    <FooterContainer>
      <FooterCopy>
        <p>Copyright 2019 &copy; All rights reserved</p>
        <div>
          <p>Made with tea and late nights</p>
          <FlexCenter>
            <i className="fas fa-mug-hot" />
            <i className="fas fa-moon" />
          </FlexCenter>
        </div>
      </FooterCopy>
      <FooterIconWrapper>
        <p>Follow us on: </p>
        <div>
          <button>
            <i className="fab fa-instagram" />
          </button>
          <button>
            <i className="fab fa-twitter" />
          </button>
          <button>
            <i className="fab fa-github" />
          </button>
        </div>
      </FooterIconWrapper>
    </FooterContainer>
  )
}

export default Footer
