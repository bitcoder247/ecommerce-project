import React from "react"
import { css } from "@emotion/core"
import Showcase from "../showcase/showcase"
import { Head } from "../../styles/showcase/header"

const Header = () => {
  return (
    <Head>
      <Showcase />
    </Head>
  )
}

export default Header

/*
linear-gradient(#285bcd, #193A85)
#193A85
#002578
*/
