import React from "react"
import { css, Global } from "@emotion/core"
import PropTypes from "prop-types"
import { useStaticQuery, graphql } from "gatsby"
import { Helmet } from "react-helmet"

const Layout = ({ children }) => {
  const data = useStaticQuery(graphql`
    query SiteTitleQuery {
      site {
        siteMetadata {
          title
          author
          description
        }
      }
    }
  `)

  return (
    <>
      <Helmet>
        <meta charSet="utf-8" />
        <title>{data.site.siteMetadata.title}</title>
        <meta name="author" content={data.site.siteMetadata.author} />
        <meta name="description" content={data.site.siteMetadata.description} />
        <link
          rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.11.2/css/all.css"
        ></link>
        <link
          rel="stylesheet"
          href="https://use.fontawesome.com/releases/v5.11.2/css/v4-shims.css"
        ></link>
      </Helmet>
      <Global
        styles={css`
          * {
            box-sizing: border-box;
            margin: 0;
            padding: 0;
            overflow-x: hidden;
          }

          html,
          body {
            font-family: 'Loto', sans-serif;
            margin: 0;
            font-size: 1rem;
            line-height: 1.5;
          }

          a {
            text-decoration: none;
            cursor: pointer;
          }

          li {
            list-style: none;
          }
          
          select,
          button {
            border: none;
            cursor: pointer;
            padding: 0;
            background-color: transparent;
          }

          input,
          textarea {
            none
          }

        `}
      />
      {children}
    </>
  )
}

Layout.propTypes = {
  children: PropTypes.node.isRequired,
}

export default Layout
