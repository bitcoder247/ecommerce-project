import React, { useState, useEffect } from "react"
import { CSSTransition } from "react-transition-group"
import styled from "@emotion/styled"
import Image from "gatsby-image"

import {
  ProductLink,
  CardContainer,
  ImageContainer,
  ProductDescription,
  ProductPurchaseDetails,
  PriceDisplay,
  PopUpDisplay,
  PopUp,
} from "../../styles/product/productCardStyles"
import { FlexCenter } from "../../styles/general/generalStyles"

const Img = styled(Image)`
  width: 100%;
  height: 100%;
  transform: scale(1);
  &.zoom-enter {
    transform: scale(1);
  }
  &.zoom-enter-active {
    transform: scale(1.5);
    transition: transform 400ms ease-in-out;
  }

  &.zoom-enter-done {
    transform: scale(1.5);
  }

  &.zoom-exit {
    transform: scale(1.5);
  }

  &.zoom-exit-active {
    transform: scale(1);
    transition: transform 400ms ease-in-out;
  }
`

const ProductCard = props => {
  const [trigger, setTrigger] = useState(false)
  const [hover, setHover] = useState(false)

  const { image, name, description, currency, price, product } = props
  let handle = name
    .toLowerCase()
    .split(" ")
    .join("-")

  const priceFormatted = new Intl.NumberFormat("en-US", {
    style: "currency",
    currency: "USD",
  }).format(price * 0.01)

  const handleClick = () => {
    sessionStorage.clear()
    setTrigger(true)
  }

  useEffect(() => {
    if (trigger === true) {
      sessionStorage.setItem("productData", JSON.stringify(product))
    }

    return () => {}

    // Didn't thoroughly checked what happens if product is added as a dependency
  }, [trigger, product])

  return (
    <ProductLink
      to={`/product/${handle}`}
      onMouseOver={() => setHover(true)}
      onMouseLeave={() => setHover(false)}
      onClick={() => handleClick()}
    >
      <CardContainer>
        <ImageContainer>
          <CSSTransition in={hover} timeout={400} classNames={"zoom"}>
            <Img
              fluid={image}
              style={{
                width: "100%",
                height: "100%",
              }}
            />
          </CSSTransition>
        </ImageContainer>
        <ProductDescription>
          <h3>{name}</h3>
          <p>{description}</p>
        </ProductDescription>
        <ProductPurchaseDetails>
          <PriceDisplay>
            <FlexCenter>
              <h3>{currency.toUpperCase()}</h3>
              <h3>{priceFormatted}</h3>
            </FlexCenter>
          </PriceDisplay>
          <CSSTransition in={hover} classNames={"move"} timeout={300}>
            <PopUpDisplay>
              <div>
                <p>view details</p>
                <p>&amp; buy</p>
              </div>
              <div>
                <i className="fas fa-arrow-right" />
              </div>

              <PopUp>
                <i className="fas fa-shopping-cart" />
              </PopUp>
            </PopUpDisplay>
          </CSSTransition>
        </ProductPurchaseDetails>
      </CardContainer>
    </ProductLink>
  )
}

export default ProductCard
