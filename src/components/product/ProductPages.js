import React, { useContext, useState, useEffect } from "react"
import {navigate} from "gatsby"
import styled from "@emotion/styled"
import Image from "gatsby-image"
import { CSSTransition } from "react-transition-group"

import ShopContext from "../context/ShopContext"
import Layout from "../layout/layout"
import { Logo } from "../../styles/shop/shopNav"
import ShopCartCheckout from "../shop/shopcartcheckout"

import ProductZoomImage from "./ProductZoomImage"

import {
  ProductSection,
  ProductNav,
  ProductLogoContainer,
  ProductBody,
  ProductInfo,
  ProductTitle,
  ProductDescription,
  ProductPrice,
  ProductSelectionContainer,
  ImageContainer,
  ImageButton,
  BackButton,
  ThumbnailContainer,
  Thumbnail,
  ThumbnailButton,
  SizeSelection,
  QuantityInput,
  AddCartButton,
  ButtonContainer,
  ErrMessage,
  FooterContainer,
} from "../../styles/product/productPageStyles"

const Img = styled(Image)`
  overflow: hidden;
  padding: 0;
  width: 100%;
  height: 100%;
  transform: scale(1);

  &.zoom-enter {
    transform: scale(1);
  }
  &.zoom-enter-active {
    transform: scale(1.5);
    transition: transform 400ms ease-in-out;
  }

  &.zoom-enter-done {
    transform: scale(1.5);
  }

  &.zoom-exit {
    transform: scale(1.5);
  }

  &.zoom-exit-active {
    transform: scale(1);
    transition: transform 400ms ease-in-out;
  }
`

const ProductPages = () => {
  const [state, setState] = useState({
    imageObj: null,
    imageInd: null,
    data: undefined,
    quantity: 1,
    size: "Choose Size",
  })
  const [hover, setHover] = useState(false)
  const [trigger, setTrigger] = useState(false)
  const [sizeErr, setSizeErr] = useState(false)
  
  const shopContext = useContext(ShopContext)

  const { addProduct, clearCart } = shopContext

  const handleSubmit = e => {
    e.preventDefault()
    const quantity = parseInt(state.quantity)

    if (state.size === "Choose Size") {
      setSizeErr(true)
      return
    } else {
      addProduct({
        sku_id: state.data.id,
        product_id: state.data.product.id,
        quantity: quantity,
        size: state.size,
        fluid: { ...state.data.localFiles[0].childImageSharp.fluid },
        prodName: state.data.product.name,
        price: state.data.price,
        currency: state.data.currency,
      })
      setState({
        ...state,
        quantity: 1,
      })
    }
  }

  const handleThumbnail = (obj, index) => {
    setState({
      ...state,
      imageObj: { ...obj },
      imageInd: index,
    })
  }

  const handleGoBack = () => {
    setState({
      ...state,
      imageID: null,
      data: null,
      quantity: 1,
      size: "Choose Size",
    })
  }

  const handleHome = () => {
    navigate("/")
    setState({
      ...state,
      imageId: null,
      data: null,
      quantity: 1,
      size: "Choose Size",
    })
    clearCart()
  }


  const handleChange = event => {
    const { name, value } = event.target
    setState({
      ...state,
      [name]: value,
    })
  }

  useEffect(() => {
    setState((prevState) => {
      return {
        ...prevState,
        data: JSON.parse(sessionStorage.getItem("productData")) || ""
      }
    })
  }, [])



  useEffect(() => {
    let errMsg

    if (sizeErr === true) {
      errMsg = setTimeout(() => setSizeErr(false), 5000)
    }

    return () => {
      clearTimeout(errMsg)
    }
  }, [sizeErr])

  if (state.data === undefined || state.data === null) {
    return <div></div>
  } else {
    const priceFormatted = new Intl.NumberFormat("en-US", {
      style: "currency",
      currency: "USD",
    }).format(state.data.price * 0.01)

    if(state.data === "" || state.data === undefined) {
      navigate("/shop/")
      return (
        <div></div>
      )
    }

    return (
      <Layout>
        <ProductSection>
          <ProductNav>
            <ProductLogoContainer to="/" onClick={() => handleHome()}>
              <Logo />
            </ProductLogoContainer>
          </ProductNav>
          <ProductBody>
            <ImageContainer
              onMouseOver={() => setHover(true)}
              onMouseLeave={() => setHover(false)}
            >
              {state.imageObj !== null ? (
                <CSSTransition in={hover} timeout={400} classNames={"zoom"}>
                  <Img
                    fluid={state.imageObj}
                    style={{
                      overflow: "hidden",
                      padding: "0",
                      height: "100%",
                      width: "100%",
                    }}
                  />
                </CSSTransition>
              ) : (
                <CSSTransition in={hover} timeout={400} classNames={"zoom"}>
                  <Img
                    fluid={state.data.localFiles[0].childImageSharp.fluid}
                    style={{
                      overflow: "hidden",
                      padding: "0",
                      height: "100%",
                      width: "100%",
                    }}
                  />
                </CSSTransition>
              )}
              <ImageButton onClick={() => setTrigger(true)} />
            </ImageContainer>
            <ProductInfo>
              <BackButton to={`/shop`} onClick={() => handleGoBack()}>
                <i className="fas fa-arrow-left" />
                Back To The Shop
              </BackButton>
              <ProductTitle>
                <h2>{state.data.product.name}</h2>
              </ProductTitle>
              <div>
                <ProductDescription>
                  <h3>{state.data.product.description}</h3>
                </ProductDescription>
                <ProductPrice>
                  <h3>{state.data.currency.toUpperCase()}</h3>
                  <h3>{priceFormatted}</h3>
                </ProductPrice>
              </div>
              <ProductSelectionContainer
                onSubmit={event => handleSubmit(event)}
              >
                <div>
                  <label htmlFor="quantity">Qty:</label>
                  <QuantityInput
                    type="number"
                    id="quantity"
                    name="quantity"
                    value={state.quantity}
                    min="1"
                    max="100"
                    step="1"
                    onChange={handleChange}
                  />
                </div>
                <div>
                  <label htmlFor="size">Selection:</label>
                  <SizeSelection
                    type="select"
                    id="size"
                    name="size"
                    value={state.size}
                    onChange={handleChange}
                  >
                    <option disabled hidden>
                      Choose Size
                    </option>
                    <option value="SM - Small">SM - Small</option>
                    <option value="M - Medium">M - Medium</option>
                    <option value="L - Large">L - Large</option>
                    <option value="XL - Xtra-Large">XL - Xtra-Large</option>
                  </SizeSelection>
                </div>
                <ButtonContainer>
                  <AddCartButton type="submit">
                    Add To Cart
                    <i className="fas fa-shopping-cart" />
                  </AddCartButton>
                </ButtonContainer>
              </ProductSelectionContainer>
              <CSSTransition in={sizeErr} classNames={"warn"} timeout={400}>
                <ErrMessage
                  style={{
                    display: sizeErr === false ? "none" : "flex",
                  }}
                >
                  <i className="fas fa-exclamation-circle" />
                  You must select a size
                </ErrMessage>
              </CSSTransition>
            </ProductInfo>
            <ThumbnailContainer>
              {state.data.product.localFiles === null ? (
                <Thumbnail>
                  <Img
                    fluid={state.data.localFiles[0].childImageSharp.fluid}
                    style={{
                      overflow: "hidden",
                      padding: "0",
                      height: "100%",
                      width: "100%",
                    }}
                  />
                </Thumbnail>
              ) : (
                state.data.product.localFiles.map((imageObj, index) => {
                  if (state.imageInd === null) {
                    if (index === 0) {
                      return null
                    }
                    return (
                      <Thumbnail key={imageObj.childImageSharp.id}>
                        <Img
                          fluid={imageObj.childImageSharp.fluid}
                          style={{
                            overflow: "hidden",
                            padding: "0",
                            height: "100%",
                            width: "100%",
                          }}
                        />
                        <ThumbnailButton
                          onClick={() =>
                            handleThumbnail(
                              imageObj.childImageSharp.fluid,
                              index
                            )
                          }
                        />
                      </Thumbnail>
                    )
                  } else if (state.imageInd !== null) {
                    if (state.imageInd === index) {
                      return null
                    }
                    return (
                      <Thumbnail key={imageObj.childImageSharp.id}>
                        <Img
                          fluid={imageObj.childImageSharp.fluid}
                          style={{
                            overflow: "hidden",
                            padding: "0",
                            height: "100%",
                            width: "100%",
                          }}
                        />
                        <ThumbnailButton
                          onClick={() =>
                            handleThumbnail(
                              imageObj.childImageSharp.fluid,
                              index
                            )
                          }
                        />
                      </Thumbnail>
                    )
                  }else {
                    return null
                  }
                })
              )}
            </ThumbnailContainer>
          </ProductBody>
          <ProductZoomImage
            images={
              state.data.product.localFiles === null
                ? JSON.stringify(state.data.localFiles[0])
                : JSON.stringify(state.data.product.localFiles)
            }
            trigger={trigger}
            setTrigger={setTrigger}
            initInd={state.imageInd}
          />
          <ShopCartCheckout position={"absolute"} />
          <FooterContainer>
            <p>
              Made with love and tea
              <i className="fas fa-heart" />
              <i className="fas fa-mug-hot" />
              Check us out on github
              <i className="fab fa-github" />
            </p>
          </FooterContainer>
        </ProductSection>
      </Layout>
    )
  }
}

export default ProductPages
