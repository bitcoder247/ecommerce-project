import React, { useState } from "react"
import styled from "@emotion/styled"
import Image from "gatsby-image"
import { CSSTransition } from "react-transition-group"

import {
  ProductZoomContainer,
  BackButton,
  ImageContainer,
  ImageButton,
  SecondaryContainer,
  ThumbnailWrapper,
  ThumbnailContainer,
  Thumbnail,
  ThumbnailButton,
} from "../../styles/product/productZoomStyles"

const Img = styled(Image)`
  overflow: hidden;
  padding: 0;
  width: 100%;
  height: 100%;
  transform: scale(1);

  &.zoom-enter {
    transform: scale(1);
  }
  &.zoom-enter-active {
    transform: scale(1.5);
    transition: transform 400ms ease-in-out;
  }

  &.zoom-enter-done {
    transform: scale(1.5);
  }

  &.zoom-exit {
    transform: scale(1.5);
  }

  &.zoom-exit-active {
    transform: scale(1);
    transition: transform 400ms ease-in-out;
  }
`

const ProductZoomImage = props => {
  const [state, setState] = useState({
    zoom: false,
    imageObj: null,
    imageInd: null,
  })
  const { images, trigger, setTrigger, initInd } = props
  let imageData = JSON.parse(images)
  let firstImage
  if (Array.isArray(imageData)) {
    if (initInd) {
      firstImage = imageData[initInd].childImageSharp.fluid
    } else {
      firstImage = imageData[0].childImageSharp.fluid
    }
  } else {
    firstImage = imageData.childImageSharp.fluid
  }
  const handleGoBack = () => {
    setState({
      ...state,
      imageObj: null,
      imageInd: null,
      zoom: false,
    })
    setTrigger(false)
  }

  const handleThumbnail = (image, ind) => {
    setState({
      ...state,
      imageObj: { ...image },
      imageInd: ind,
    })
  }

  return (
    <CSSTransition in={trigger} timeout={400} classNames={"container"}>
      <ProductZoomContainer
        style={{
          display: `${trigger === false ? "none" : "flex"}`,
        }}
      >
        <ImageContainer
          style={{
            cursor: state.zoom === false ? "zoom-in" : "zoom-out",
          }}
        >
          {state.imageObj === null ? (
            <CSSTransition in={state.zoom} timeout={400} classNames={"zoom"}>
              <Img
                fluid={firstImage}
                style={{
                  overflow: "visible",
                  padding: "0",
                  height: "100%",
                  width: "100%",
                }}
              />
            </CSSTransition>
          ) : (
            <CSSTransition in={state.zoom} timeout={400} classNames={"zoom"}>
              <Img fluid={state.imageObj} />
            </CSSTransition>
          )}
          <ImageButton
            onClick={() =>
              setState({
                ...state,
                zoom: !state.zoom,
              })
            }
          />
        </ImageContainer>
        <SecondaryContainer>
          <BackButton onClick={() => handleGoBack(false)}>
            <i className="fas fa-arrow-left" />
            Back To Product
          </BackButton>
          <ThumbnailWrapper>
            {Array.isArray(imageData) === true
              ? imageData.map((imageObject, index) => {
                  if (initInd === index && state.imageInd === null) {
                    return null
                  } else if (index === 0 && initInd === null) {
                    return null
                  } else if (state.imageInd === index) {
                    return null
                  } else {
                    return (
                      <ThumbnailContainer key={imageObject.childImageSharp.id}>
                        <Thumbnail>
                          <Img fluid={imageObject.childImageSharp.fluid} />
                        </Thumbnail>
                        <ThumbnailButton
                          onClick={() =>
                            handleThumbnail(
                              imageObject.childImageSharp.fluid,
                              index
                            )
                          }
                        />
                      </ThumbnailContainer>
                    )
                  }
                })
              : null}
          </ThumbnailWrapper>
        </SecondaryContainer>
      </ProductZoomContainer>
    </CSSTransition>
  )
}

export default ProductZoomImage
