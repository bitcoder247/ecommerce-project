import React, { useState, useEffect, useContext } from "react"
import styled from "@emotion/styled"
import Image from "gatsby-image"
import { CSSTransition } from "react-transition-group"
import ShopContext from "../context/ShopContext"
import ShoppingCart from "./shoppingcart"
import {
  ShopCheckoutWrapper,
  ShopCheckOutContainer,
  CheckoutNav,
  CheckoutBody,
  NumberDisplay,
  EmptyDisplay,
  ShadowedCover,
  ItemKey,
  ItemList,
  Item,
  ItemDisplay,
  ItemForm,
  QuantityInput,
  CheckoutItem,
  ThumbnailContainer,
  Description,
  RemoveButton,
  CostContainer,
  CheckoutButton,
  NavTitle,
  GoBackButton,
  Gift,
  ShippingInfo,
  FootContainer,
} from "../../styles/shop/shopcheckout"

import { ExitButton, ExitButtonContainer } from "../../styles/shop/shoppingcart"

const Img = styled(Image)`
  width: 100%;
  height: 100%;
`

const ShopCartCheckout = props => {
  const [state, setState] = useState({
    stripe: null,
  })

  const { stripe } = state

  const shopContext = useContext(ShopContext)
  const {
    products,
    removeProduct,
    addQuantity,
    totalItems,
    clearCart,
  } = shopContext

  const total = products.reduce((acc, curr) => {
    return acc + curr.quantity * (curr.price / 100)
  }, 0)

  const [trigger, setTrigger] = useState(false)

  const handleChange = (event, product) => {
    const { value, id } = event.target
    if (product["sku_id"] === id) {
      addQuantity({ ...product }, parseInt(value))
    }
  }

  const handleDelete = id => {
    removeProduct(id)
  }

  const redirectToCheckout = async event => {
    event.preventDefault()
    let success = ""
    let cancel = ""

    if (stripe === null) return
    if(process.env.NODE_ENV === "development") {
      success = process.env.GATSBY_SUCCESS_URL_DEV
      cancel = process.env.GATSBY_CANCEL_URL_DEV
    }else {
      success = window.location.href
      cancel = window.location.href
    }

    try {
      const { error } = await stripe.redirectToCheckout({
        items: products.map(elem => {
          return {
            sku: elem["sku_id"],
            quantity: elem.quantity,
          }
        }),
        successUrl: success,
        cancelUrl: cancel,
      })

    }catch(error) {
      clearCart()
  
    } 
  }

  useEffect(() => {
    setState(prevState => {
      return {
        ...prevState,
        stripe: window.Stripe(process.env.GATSBY_STRIPE_PUBLISH_KEY),
      }
    })
  }, [])

  return (
    <CSSTransition in={trigger} timeout={400} classNames={"slide"}>
      <ShopCheckoutWrapper
        style={{
          position: props.position === "absolute" ? "absolute" : "fixed",
        }}
      >
        <ShopCheckOutContainer>
          <CheckoutNav>
            <ShoppingCart trigger={trigger} setTrigger={setTrigger} />
            <NavTitle>
              <h2>Your Cart</h2>
            </NavTitle>
            <NumberDisplay>
              <h6>items in cart</h6>
              <div>
                <h3>{`${totalItems}`}</h3>
              </div>
            </NumberDisplay>
            <ExitButtonContainer
              style={{
                display: trigger === false ? "none" : "flex",
              }}
            >
              <ExitButton onClick={() => setTrigger(false)}>
                <i className="fas fa-times" />
              </ExitButton>
            </ExitButtonContainer>
          </CheckoutNav>
          <CheckoutBody>
            {products.length === 0 ? (
              <EmptyDisplay>
                <i className="fas fa-frown" />
                <p>Your cart is empty!</p>
                <p>
                  Fill it up and buy some <br /> delicious frozen desserts.
                </p>
                <p>Nothing wrong with a sweet tooth</p>
              </EmptyDisplay>
            ) : (
              <>
                <ItemKey>
                  <span>Product</span>
                  <span>Qty:</span>
                  <span>Remove</span>
                </ItemKey>

                <ItemList>
                  {products.map(dataObj => {
                    const priceFormatted = new Intl.NumberFormat("en-US", {
                      style: "currency",
                      currency: "USD",
                    }).format(dataObj.price * 0.01)
                    return (
                      <CheckoutItem key={dataObj["sku_id"]}>
                        <Item>
                          <ItemDisplay>
                            <ThumbnailContainer>
                              <Img fluid={dataObj.fluid} />
                            </ThumbnailContainer>
                            <Description>
                              <span>{dataObj.prodName}</span>
                              <span>{`${dataObj.size}, ${priceFormatted}`}</span>
                            </Description>
                          </ItemDisplay>
                          <ItemForm>
                            <QuantityInput
                              type="number"
                              name="quantity"
                              id={dataObj["sku_id"]}
                              value={dataObj.quantity}
                              min="1"
                              max="99"
                              onChange={event => {
                                handleChange(event, dataObj)
                              }}
                            />
                            <RemoveButton
                              onClick={() => handleDelete(dataObj["sku_id"])}
                            >
                              <i className="fas fa-times" />
                            </RemoveButton>
                          </ItemForm>
                        </Item>
                      </CheckoutItem>
                    )
                  })}
                </ItemList>
              </>
            )}
            {products.length === 0 ? null : (
              <>
                <CostContainer>
                  <div>
                    <span>Subtotal:</span>
                    <span>{`USD ${total}.00`}</span>
                  </div>
                  <div>
                    <span>Taxes:</span>
                    <span>0.00</span>
                  </div>
                  <div>
                    <span>Shipping (worldwide):</span>
                    <span>FREE</span>
                  </div>
                  <div>
                    <strong>TOTAL PRICE:</strong>
                    <span>{`USD ${total}.00`}</span>
                  </div>
                </CostContainer>
                <GoBackButton to={"/shop"}>
                  <i className="fas fa-arrow-left" />
                  Back To The Shop
                </GoBackButton>
                <CheckoutButton onClick={event => redirectToCheckout(event)}>
                  Checkout
                  <i className="fas fa-cash-register" />
                </CheckoutButton>
                <Gift>
                  <p>
                    <i className="fas fa-smile-beam" />
                    We will add a cute sticker <br /> as part of your order!
                  </p>
                  <i className="fas fa-certificate" />
                </Gift>
                <ShippingInfo>
                  <p>
                    International shipments{" "}
                    <span>can take up to 6 weeks or more</span> to be delivered
                    and may be subject to local taxes and duties.
                  </p>
                </ShippingInfo>
                <FootContainer>
                  <p>
                    Made with love and tea
                    <i className="fas fa-heart" />
                    <i className="fas fa-mug-hot" />
                    Check us out on github
                    <i className="fab fa-github" />
                  </p>
                </FootContainer>
              </>
            )}
          </CheckoutBody>
          <ShadowedCover
            style={{
              display: trigger === false ? "none" : "block",
            }}
          />
        </ShopCheckOutContainer>
      </ShopCheckoutWrapper>
    </CSSTransition>
  )
}

export default ShopCartCheckout
