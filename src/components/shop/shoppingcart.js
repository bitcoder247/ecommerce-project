import React, { useContext } from "react"
import { CSSTransition } from "react-transition-group"
import ShopContext from "../context/ShopContext"
import {
  ButtonContainer,
  ShopButton,
  NumberDisplay,
} from "../../styles/shop/shoppingcart"

const ShoppingCart = props => {
  const shopContext = useContext(ShopContext)
  const { display, products, totalItems } = shopContext
  const { trigger, setTrigger } = props

  return (
    <ButtonContainer
      style={{
        display: trigger === false ? "flex" : "none",
      }}
    >
      <ShopButton onClick={() => setTrigger(true)}>
        <i className="fas fa-shopping-cart" />
        <CSSTransition
          in={display}
          appear={display}
          timeout={300}
          classNames={"display"}
        >
          <NumberDisplay
            style={{
              display: products.length === 0 ? "none" : "flex",
            }}
          >
            {products.length === 0 ? null : totalItems}
          </NumberDisplay>
        </CSSTransition>
      </ShopButton>
    </ButtonContainer>
  )
}

export default ShoppingCart
