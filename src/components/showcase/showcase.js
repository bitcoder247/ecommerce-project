import React from "react"
import { Link } from "gatsby"
import {
  GridContainer,
  FlexCenter,
} from "../../styles/general/generalStyles"

import { Header } from "../../styles/showcase/header"

import {
  ShowcaseButtonContainer,
  ShowcaseContainer,
  ShowcaseFlexWrapper,
  LogoContainer,
  SVGContainer,
  DividerContainer,
  ShowcaseIconWrapper,
  ShowcaseActionContainer,
  ShowcaseFooterContainer,
} from "../../styles/showcase/showcaseContainers"

import {
  ShowcaseButton,
  ShowcaseLink,
  ShowcaseCenterOne,
  ShowcasePitch,
  HorizontalBar,
  ShowcaseCenterTwo,
  ConeSvg,
  AbsoluteLargeCircle,
  Triangle,
  SnowSVG,
  IcicleSVG,

} from "../../styles/showcase/showcaseBody"

import { ShowcaseNav, ShoppingCart,   LogoSVG } from "../../styles/showcase/showcaseNav"

const Showcase = ({ familyScroll, contactScroll }) => {
  return (
    <Header>
      <GridContainer>
        <AbsoluteLargeCircle />
        <SVGContainer>
          <ConeSvg />
        </SVGContainer>
        <ShowcaseContainer>
          <ShowcaseNav>
              <ShowcaseFlexWrapper>
                <LogoContainer>
                  <LogoSVG />
                </LogoContainer>
                <ShoppingCart>
                  <Link to="/shop">
                    <i className="fas fa-store-alt" />
                  </Link>
                </ShoppingCart>
              </ShowcaseFlexWrapper>
          </ShowcaseNav>
          <ShowcaseCenterOne>
            <ShowcasePitch>
              <h1>Delicious n' Sweet</h1>
              <h1>For Any Occasion</h1>
              <DividerContainer>
                <HorizontalBar />
                <ShowcaseIconWrapper>
                  <i className="fas fa-ice-cream" />
                </ShowcaseIconWrapper>
                <HorizontalBar />
              </DividerContainer>
              <em>"Satisfy that cold sweet tooth"</em>
            </ShowcasePitch>
            <ShowcaseButtonContainer>
              <ul>
                <li>
                  <div>
                    <ShowcaseButton>
                      <ShowcaseLink to="/shop">
                        Our Shop
                      </ShowcaseLink>
                    </ShowcaseButton>
                    <SnowSVG>
                      <path
                        className="melt"
                        d="M0 0 L0 10 c 0 0, 0 -5, 50 -10 Z"
                      />
                    </SnowSVG>
                    <IcicleSVG>
                      <polygon className="melt" points="0,0 10,0 5,20" />
                      <polygon className="melt" points="18,0 26,0 22,15" />
                      <polygon className="melt" points="40,0 48,0 44,25" />
                    </IcicleSVG>
                  </div>
                </li>
                <li>
                  <div>
                    <ShowcaseButton onClick={() => contactScroll()}>
                      Contact Us
                    </ShowcaseButton>
                    <SnowSVG>
                      <path d="M0 0 L0 10 c 0 0, 0 -2, 60 -10 Z" />
                    </SnowSVG>
                    <IcicleSVG>
                      <polygon points="0,0 8,0 4,20" />
                      <polygon points="18,0 26,0 22,15" />
                      <polygon points="40,0 48,0 44,25" />
                      <polygon points="70,0 78,0 74,20" />
                    </IcicleSVG>
                  </div>
                </li>
              </ul>
            </ShowcaseButtonContainer>
            <ShowcaseActionContainer>
              <div>
                <ShowcaseButton onClick={() => familyScroll()}>
                  Find Out More
                </ShowcaseButton>
                <SnowSVG>
                  <path d="M0 0 L0 10 c 0 0, 0 0, 50 -10 Z" />
                </SnowSVG>
                <IcicleSVG>
                  <polygon points="0,0 10,0 5,20" />
                  <polygon points="18,0 26,0 22,15" />
                  <polygon points="40,0 48,0 44,25" />
                  <polygon points="70,0 78,0 74,20" />
                  <polygon points="92,0 100,0 95,20" />
                </IcicleSVG>
              </div>
            </ShowcaseActionContainer>
          </ShowcaseCenterOne>
          <ShowcaseCenterTwo>
            <Triangle />
            <ShowcaseFooterContainer>
              <FlexCenter>
                <p>Follow Us On: </p>
              </FlexCenter>
              <FlexCenter>
                <button>
                  <i className="fab fa-facebook" />
                </button>
                <button>
                  <i className="fab fa-twitter" />
                </button>
                <button>
                  <i className="fab fa-instagram" />
                </button>
                <button>
                  <i className="fab fa-snapchat" />
                </button>
              </FlexCenter>
            </ShowcaseFooterContainer>
          </ShowcaseCenterTwo>
        </ShowcaseContainer>
      </GridContainer>
    </Header>
  )
}

export default Showcase
