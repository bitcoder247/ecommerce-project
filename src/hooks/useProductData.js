import { graphql, useStaticQuery } from "gatsby"

const useProductData = () => {
  const data = useStaticQuery(graphql`
    query {
      stripeData: allStripeSku(sort: { fields: [product___name], order: ASC }) {
        skus: edges {
          sku: node {
            id
            product: product {
              id
              name
              imgobj: localFiles {
                img: childImageSharp {
                  fluid {
                    src
                  }
                }
              }
            }
            skuImgObj: localFiles {
              skuImg: childImageSharp {
                fluid {
                  src
                }
              }
            }
          }
        }
      }
    }
  `)

  return [[...data.stripeData.skus]]
}

export default useProductData
