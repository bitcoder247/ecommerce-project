import React from "react"

import useScroll from "../hooks/useScroll"

import Layout from "../components/layout/layout"
import Showcase from "../components/showcase/showcase"
import Family from "../components/family/family"
import Business from "../components/business/business"
import Customized from "../components/customized/customized"
import Contact from "../components/contact/contact"
import Footer from "../components/footer/footer"

const IndexPage = () => {
  const [familyRef, familyScroll] = useScroll()
  const [contactRef, contactScroll] = useScroll()

  return (
    <Layout>
      <Showcase familyScroll={familyScroll} contactScroll={contactScroll} />
      <Family ref={familyRef} />
      <Business contactScroll={contactScroll} />
      <Customized contactScroll={contactScroll} />
      <Contact ref={contactRef} />
      <Footer />
    </Layout>
  )
}

export default IndexPage
