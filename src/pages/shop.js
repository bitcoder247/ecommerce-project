import React from "react"
import { graphql, useStaticQuery } from "gatsby"
import Layout from "../components/layout/layout"

import ShopCartCheckout from "../components/shop/shopcartcheckout"
import ProductCard from "../components/product/ProductCard"

import { NavContainer, ShopNav, ShopLogoContainer, Logo } from "../styles/shop/shopNav"
import {
  ShopContainer,
  ShopGridContainer,
  FootContainer,
} from "../styles/shop/shopBody"

const ShopPage = () => {
  const data = useStaticQuery(graphql`
    query {
      allStripeSku(sort: { fields: [product___name], order: ASC }) {
        edges {
          node {
            id
            currency
            price
            localFiles {
              childImageSharp {
                fluid {
                  ...GatsbyImageSharpFluid
                }
                fixed(width: 120) {
                  ...GatsbyImageSharpFixed
                }
              }
            }
            product {
              id
              name
              description
              localFiles {
                childImageSharp {
                  id
                  fluid {
                    ...GatsbyImageSharpFluid
                  }
                }
              }
            }
          }
        }
      }
    }
  `)

  return (
    <Layout>
      <ShopContainer>
        <ShopNav>
          <NavContainer>
            <ShopLogoContainer to="/">
              <Logo />
            </ShopLogoContainer>
          </NavContainer>
        </ShopNav>
        <ShopGridContainer>
          {data.allStripeSku.edges.map(data => {
            return (
              <ProductCard
                key={data.node.id}
                name={data.node.product.name}
                image={data.node.localFiles[0].childImageSharp.fluid}
                price={data.node.price}
                currency={data.node.currency}
                description={data.node.product.description}
                productID={data.node.product.id}
                product={data.node}
              />
            )
          })}
        </ShopGridContainer>
        <FootContainer>
          <p>
            Made with love and tea
            <i className="fas fa-heart" />
            <i className="fas fa-mug-hot" />
            Check us out on github
            <i className="fab fa-github" />
          </p>
        </FootContainer>
        <ShopCartCheckout />
      </ShopContainer>
    </Layout>
  )
}

export default ShopPage
