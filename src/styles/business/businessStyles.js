import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"
import polarbear from "../../../content/polarbear.svg"

/* width: 100px height: 120px */
export const PolarBear = styled(polarbear)`
  ${mq("iphoneSE", "min")} {
    width: 200px;
    height: 240px;
  }

  ${mq("desktop", "min")} {
    width: 400px;
    height: 480px;
  }
`
