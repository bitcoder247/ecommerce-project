import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"

export const FormContainer = styled("div")`
  ${mq("iphoneSE", "min")} {
    width: 100%;
    height: 100%;
  }

  ${mq("desktop", "min")} {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 50%;
    height: 100%;
  }
`

export const Form = styled("form")`
  position: relative;
  width: 100%;
  padding: 1.5rem;
  z-index: 0;
  label {
    display: block;
    color: #eee;
    font-weight: 700;
    margin-bottom: 0.4rem;
  }
  input {
    width: 90%;
    padding: 0.5rem 1.2rem;
    margin-bottom: 1rem;
    border: 1px solid rgba(0, 0, 0, 0.2);
  }

  div {
    display: flex;
    justify-content: flex-start;
    align-items: center;
  }
`
