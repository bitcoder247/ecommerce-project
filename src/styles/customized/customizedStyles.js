import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"
import swan from "../../../content/swan_opt_01.svg"

/* width: 222px height: 135 */
export const Swan = styled(swan)`
  ${mq("desktop", "min")} {
    height: 270px;
    width: 444px;
  }
`
