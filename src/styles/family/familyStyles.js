import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"
import family from "../../../content/family_opt_01.svg"

export const TextContainer = styled("div")`
  color: #eee;
  text-align: center;
  overflow: hidden;

  h2 {
    font-family: "Baloo";
  }

  p {
    font-weight: 700;
  }

  ${mq("iphoneSE", "min")} {
    width: 100%;
    h2 {
      padding: 0.2rem 0.75rem;
      margin: 1rem 0;
    }
    p {
      padding: 0.2rem 1rem;
      margin: 1rem 0;
    }
  }

  ${mq("iphoneSE", "min")} and (orientation: landscape) {
    p {
      padding: 1rem 1rem;
      margin: 1rem 2rem;
    }
  }

  ${mq("desktop", "min")} {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    height: 100%;
    width: 50%;
    h2 {
      font-size: 2.5rem;
      margin: 0.3rem 0;
    }
  }
`

/* width: 72px height: 100px */
export const FamilySVG = styled(family)`
  ${mq("iphoneSE", "min")} {
    width: 216px;
    height: 300px;
  }
  ${mq("desktop", "min")} {
    width: 360px;
    height: 500px;
  }
`
