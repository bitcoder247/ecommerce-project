import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"

export const FooterContainer = styled("div")`
  display: flex;
  align-items: center;
  width: 100%;
  height: 100%;
  background-color: #00216c;
  padding: 1rem;

  ${mq("iphoneSE", "min")} {
    justify-content: center;
    flex-direction: column;
  }

  ${mq("tablet", "min")} {
    justify-content: center;
    flex-direction: column;
  }
  ${mq("phoneLg", "min")} and (orientiation: landscape) {
    justify-content: center;
    flex-direction: column;
  }
  ${mq("desktop", "min")} {
    justify-content: center;
    flex-direction: row;
  }
`

export const FooterCopy = styled("div")`
  display: flex;
  justify-content: space-around;
  color: #eee;
  font-weight: 700;
  div {
    display: flex;
    justify-content: center;
    align-items: center;
    i {
      margin: 0 0.5rem;
    }
  }
  ${mq("iphoneSE", "min")} {
    width: 100%;
    flex-direction: column;
    font-size: 0.8rem;
    align-items: center;
    margin-bottom: 1rem;
  }
  ${mq("iphoneSE", "min")} and (orientation: landscape) {
    align-items: center;
    flex-direction: row;
  }

  ${mq("desktop", "min")} {
    width: 50%;
    font-size: 1rem;
    margin: 0;
    flex-direction: column;
  }
`

export const FooterIconWrapper = styled("div")`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  color: #eee;
  div {
    display: flex;
    justify-content: center;
    align-items: center;
    button {
      display: inline-block;
      color: #eee;
      padding: 0.5rem;
      transition: all 300ms ease-in-out;
      i {
        font-size: 1rem;
        margin: 0 1rem;
      }
      &:hover {
        color: #f7d30b;
      }
    }
  }

  ${mq("iphoneSE", "min")} {
    justify-content: center;
    width: 100%;
    font-size: 1.2rem;

    p {
      font-size: 1rem;
      font-weight: 700;
    }
  }

  ${mq("iphoneSE", "min")} and (orientation: landscape) {
    margin-top: 1rem;
  }

  ${mq("desktop", "min")} {
    width: 50%;
    height: 100%;
    font-size: 2rem;
    margin: 0;
    p {
      font-size: 1rem;
      font-weight: 700;
    }
  }
`
