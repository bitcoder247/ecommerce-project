import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"

export const GridContainer = styled("div")`
  position: relative;
  display: grid;
  width: 100%;
  height: 100%;
  grid-template-columns: repeat(12, 1fr);
  grid-template-rows: 80px auto;
`

export const MaxContainer = styled("div")`
  width: 100%;
  ${mq("desktop", "min")} {
    max-width: 1200px;
    margin: 0 auto;
  }
`

export const FlexCenter = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
`
export const FlexBetween = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
`

export const SectionContainer = styled("section")`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  background-color: #00216c;
  margin: -1px 0 0 0;

  ${mq("iphoneSE", "min")} {
    flex-direction: column;
    width: 100vw;
    height: 100%;
    padding: 1rem 0;
  }

  ${mq("iphoneSE", "min")} and (orientation: landscape) {
    height: 100%;
  }

  ${mq("tablet", "min")} {
    height: 100vh;
    div {
      width: 50%;
    }
  }

  ${mq("tablet", "min")} and (orientation: landscape) {
    height: 100%;
  }

  ${mq("desktop", "min")} {
    width: 100vw;
    height: 100vh;
    flex-direction: row-reverse;
    div {
      width: 50%;
    }
  }
`

export const SVGContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;

  ${mq("iphoneSE", "min")} {
    padding: 1rem;
  }
`

export const GeneralButton = styled("button")`
  position: relative;
  padding: 0.75rem 1.2rem;
  background-color: #285bcd;
  color: #eee;
  font-weight: 700;
  overflow: hidden;
  transition: all 500ms ease-in-out;

  &::before {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    transform: scale(0);
    transition: all 500ms ease-in-out;
    border-radius: 50%;
    transform-origin: center;
    z-index: -1;
  }

  &:hover::before {
    background-color: #f7d30b;
    transform: scale(2);
  }

  &:hover {
    color: #000;
    background-color: transparent;
  }
`

export const TextContainer = styled("div")`
  position: relative;
  color: #eee;
  text-align: center;
  overflow: visible;
  z-index: 0;
  h2 {
    font-family: "Baloo";
  }

  p {
    font-weight: 700;
  }

  ${mq("iphoneSE", "min")} {
    width: 100%;
    h2 {
      padding: 0.2rem 0.75rem;
      margin: 1rem 0;
    }
    p {
      padding: 0.2rem 1rem;
      margin: 1rem 0;
    }
  }
  ${mq("iphoneSE", "min")} and (orientation: landscape) {
    p {
      padding: 1rem 1rem;
      margin: 1rem 2rem;
    }
  }

  ${mq("desktop", "min")} {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    height: 100%;
    width: 50%;
    h2 {
      font-size: 2.5rem;
      margin: 0.3rem 0;
    }
  }
`
