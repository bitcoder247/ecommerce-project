import { Link } from "gatsby"
import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"

export const CardContainer = styled("div")`
  display: flex;
  flex-direction: column;
  width: 100%;
  height: 100%;
  padding-top: 1.5rem;
  background-color: rgba(240, 240, 240, 0.5);
`
export const ProductLink = styled(Link)`
  display: block;
  width: 100%;
  box-shadow: 2px 10px 10px 5px #eee;

  ${mq("iphoneSE", "min")} {
    grid-column-start: 1;
    grid-column-end: 13;
  }

  ${mq("phoneLg", "min")} {
    grid-column-start: 3;
    grid-column-end: 11;
  }

  ${mq("tablet", "min")} {
    &:nth-of-type(odd) {
      grid-column-start: 1;
      grid-column-end: 7;
    }

    &:nth-of-type(even) {
      grid-column-start: 7;
      grid-column-end: 13;
    }
  }

  ${mq("tabletLg", "min")} {
    &:nth-of-type(odd) {
      grid-column-start: 2;
      grid-column-end: 7;
    }

    &:nth-of-type(even) {
      grid-column-start: 7;
      grid-column-end: 12;
    }
  }

  ${mq("desktop", "min")} {
    &:nth-of-type(odd) {
      grid-column-start: 2;
      grid-column-end: 6;
    }
    &:nth-of-type(even) {
      grid-column-start: 8;
      grid-column-end: 12;
    }
  }
`

export const ImageContainer = styled("div")`
  overflow: hidden;
  width: 100%;

  ${mq("iphoneSE", "min")} {
    height: 350px;
  }
  ${mq("desktop", "min")} {
    height: 350px;
  }
`
export const ProductDescription = styled("div")`
  width: 100%;
  padding: 1rem;
  font-weight: 700;
  color: #002578;
  h3 {
    font-size: 1.5rem;
    text-align: left;
    margin-bottom: 0.4rem;
    padding: 0.5rem;
    border-bottom: 2px solid #002578;
  }
  p {
    color: #333;
    font-size: 0.9rem;
    padding: 0.5rem;
    text-align: left;
  }
`
export const ProductPurchaseDetails = styled("div")`
  display: flex;
  align-items: flex-end;
  justify-content: space-between;
  flex-grow: 1;
  padding: 1rem 0.5rem;

  ${mq("iphoneSE", "min")} {
    flex-direction: column;
  }
  ${mq("phoneLg", "min")} {
    flex-direction: row;
  }
`
export const PriceDisplay = styled("div")`
  font-size: 1.25rem;
  h3:nth-of-type(1) {
    color: #ccc;
  }
  h3:nth-of-type(2) {
    color: #002578;
    font-weight: 700;
    margin-left: 0.3rem;
  }
  ${mq("iphoneSE", "min")} {
    margin-bottom: 0.3rem;
  }
`
export const PopUpDisplay = styled("div")`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #002578;
  transform: translateX(0);
  overflow: visible;
  div:nth-of-type(1) {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    p {
      align-self: flex-end;
      font-size: 0.8rem;
      color: #002578;
    }
  }

  div:nth-of-type(2) {
    font-size: 0.9rem;
    padding: 0.5rem 0 0.5rem 0.5rem;
    margin-left: 0.3rem;
  }

  &.move-enter {
    transform: translateX(0);
  }

  &.move-enter-active {
    transform: translateX(-50px);
    transition: transform 300ms ease-in-out;
  }
  &.move-enter-done {
    transform: translateX(-50px);
  }
  &.move-exit {
    transform: translateX(-50px);
  }
  &.move-exit-active {
    transform: translateX(0);
    transition: transform 300ms ease-in-out;
  }
`

export const PopUp = styled("div")`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 1.3rem;
  height: 100%;
  width: 50px;
  right: -50px;
  transform: translateX(16px);
  background-color: #002578;
  color: #fff;
`
