import { Link } from "gatsby"
import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"

export const ProductSection = styled("section")`
  position: relative;
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  grid-template-rows: 60px auto;
  overflow: hidden;
  z-index: 1;
  height: 100%;
  min-height: 100vh;
`

export const ProductNav = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  position: relative;
  grid-column-start: 1;
  grid-column-end: 13;
  grid-row-start: 1;
  grid-row-end: 2;
  background-color: #002578;
  z-index: 2;
  overflow: hidden;

  ${mq("desktop", "min")} {
    width: 100%;
  }
`

export const ProductLogoContainer = styled("button")`
  display: flex;
  justify-content: center;
  align-items: center;  
  height: 100%;
  padding: 0.3rem;
`

export const ProductBody = styled("div")`
  position: relative;
  grid-column-start: 1;
  grid-column-end: 13;
  grid-row-start: 2;
  z-index: 1;
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  padding-top: 1.5rem;

  ${mq("desktop", "min")} {
    grid-template-rows: 400px 110px 100px;
  }
`

export const ImageContainer = styled("div")`
  position: relative;
  overflow: hidden;
  box-shadow: 3px 3px 15px #ccc;

  ${mq("iphoneSE", "min")} {
    grid-column-start: 2;
    grid-column-end: 12;
  }

  ${mq("tablet", "min")} {
    grid-column-start: 3;
    grid-column-end: 11;
  }

  ${mq("desktop", "min")} {
    grid-column-start: 4;
    grid-column-end: 7;
    grid-row-start: 1;
    grid-row-end: 2;
    height: 375px;
  }
`

export const ImageButton = styled("button")`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`

export const BackButton = styled(Link)`
  display: flex;
  justify-content: center;
  align-items: center;
  border: 2px solid #002578;
  font-size: 1.2rem;
  color: #002578;
  height: 40px;
  transition: all 300ms ease-in-out;
  padding: 0.5rem 1rem;
  overflow: hidden;
  width: 60%;
  i {
    margin-right: 0.3rem;
  }
  &:hover {
    background-color: #002578;
    color: #fff;
  }

  ${mq("iphoneSE", "min")} {
    width: 100%;
  }

  ${mq("desktop", "min")} {
    margin-right: auto;
  }
`

export const ThumbnailContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;

  ${mq("iphoneSE", "min")} {
    grid-column-start: 2;
    grid-column-end: 12;
  }

  ${mq("desktop", "min")} {
    grid-column-start: 4;
    grid-column-end: 7;
    margin-top: 0.5rem;
  }
`

export const Thumbnail = styled("div")`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100px;
  height: 100px;
  padding: 0.5rem;
  overflow: hidden;
`

export const ThumbnailButton = styled("button")`
  position: absolute;
  width: 100%;
  height: 100%;
`

export const ProductInfo = styled("div")`
  position: relative;
  overflow: hidden;
  ${mq("iphoneSE", "min")} {
    grid-column-start: 2;
    grid-column-end: 12;
    margin-top: 1rem;
  }

  ${mq("tablet", "min")} {
    grid-column-start: 3;
    grid-column-end: 11;
  }

  ${mq("desktop", "min")} {
    grid-column-start: 8;
    grid-column-end: 12;
    grid-row-start: 1;
    grid-row-end: 4;
  }
`

export const ProductTitle = styled("div")`
  margin-top: 1.5rem;
  color: #002578;
  font-size: 1.1rem;
  font-weight: 700;
  width: 100%;

  ${mq("desktop", "min")} {
    padding: 0.5rem 0;
    text-align: left;
  }
`

export const ProductDescription = styled("div")`
  color: #333;
  font-size: 1rem;
  font-weight: 700;

  ${mq("desktop", "min")} {
    padding: 0.5rem 0;
  }
`
export const ProductPrice = styled("div")`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  padding: 0.5rem 0.5rem 0.5rem 0;
  font-size: 1rem;
  font-weight: 700;
  width: 100%;
  color: #002578;

  ${mq("desktop", "min")} {
    h3:nth-of-type(1) {
      margin-right: 0.3rem;
    }
  }
`

export const ProductSelectionContainer = styled("form")`
  display: flex;
  width: 100%;

  ${mq("iphoneSE", "min")} {
    flex-direction: column;
    div {
      margin: 1rem 0;
      width: 100%;
      label {
        display: block;
        font-size: 1.1rem;
        font-weight: 700;
        margin-bottom: 0.4rem;
        color: #333;
      }
    }
  }

  ${mq("tablet", "min")} {
    flex-direction: column;
    align-items: center;
    div {
      margin: 1rem;
    }
  }

  ${mq("desktop", "min")} {
    justify-content: space-between;
    align-items: center;
    flex-direction: column;
    div {
      width: 100%;
      margin: 1rem 0;
      label {
        display: block;
        font-size: 1.1rem;
        font-weight: 700;
        margin-bottom: 0.4rem;
        color: #333;
      }
    }
  }
`
export const QuantityInput = styled("input")`
  padding: 0.5rem 1rem;
  border: 2px #002578 solid;
  ${mq("iphoneSE", "min")} {
    width: 100%;
  }

  ${mq("desktop", "min")} {
    width: 30%;
  }
`

export const SizeSelection = styled("select")`
  padding: 0.5rem 1rem;
  border: 2px #002578 solid;

  ${mq("iphoneSE", "min")} {
    width: 100%;
  }

  ${mq("desktop", "min")} {
    width: 70%;
    margin-right: 5rem;
  }
`
export const AddCartButton = styled("button")`
  background-color: #002578;
  font-size: 1.2rem;
  font-weight: 700;
  color: #fff;
  padding: 0.75rem 1.2rem;
  transition: all 300ms ease-in-out;
  margin-top: 0.3rem;

  i {
    margin-left: 0.3rem;
  }
  &:hover {
    background-color: #f7d30b;
    color: #000;
    border: 1px solid #000;
  }
`

export const ButtonContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
`

export const ErrMessage = styled("div")`
  position: absolute;
  justify-content: center;
  align-items: center;
  top: 0;
  right: 0;
  padding: 0.5rem;
  background-color: #cb0b0b;
  border: 1px solid #000;
  color: #fff;
  font-size: 1.1rem;
  font-weight: 700;
  opacity: 0;
  transform: scale(0);
  i {
    padding: 0.5rem;
    margin-right: 0.3rem;
    border-right: 2px solid #fff;
  }

  &.warn-enter {
    opacity: 0;
    transform: scale(0);
  }

  &.warn-enter-active {
    opacity: 1;
    transform: scale(1);
    transition: all 400ms ease-in-out;
  }

  &.warn-enter-done {
    opacity: 1;
    transform: scale(1);
  }

  &.warn-exit {
    opacity: 1;
    transform: scale(1);
  }

  &.warn-exit-active {
    opacity: 1;
    transform: scale(0);
    transition: all 400ms ease-in-out;
  }
`

export const FooterContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;

  p {
    color: #002578;
    i {
      margin: 0 0.5rem;
    }
  }

  ${mq("iphoneSE", "min")} {
    grid-column-start: 1;
    grid-column-end: 13;
    margin: 1rem 0;
    padding: 1rem;
  }

  ${mq("desktop", "min")} {
    grid-column-start: 8;
    grid-column-end: 12;
    grid-row-start: 4;
    grid-row-end: 5;
  }
`
