import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"

export const ProductZoomContainer = styled("div")`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
  background-color: #fff;
  transform: scale(0);
  opacity: 0;
  z-index: 5;
  &.container-enter {
    transform: scale(0);
    opacity: 0;
  }
  &.container-enter-active {
    transform: scale(1);
    opacity: 1;
    transition: all 400ms ease-in-out;
  }
  &.container-enter-done {
    transform: scale(1);
    opacity: 1;
  }
  &.container-exit {
    transform: scale(1);
    opacity: 1;
  }
  &.container-exit-active {
    transform: scale(0);
    opacity: 0;
    transition: all 400ms ease-in-out;
  }
  ${mq("iphoneSE", "min")} {
    justify-content: flex-start;
    align-items: center;
    flex-direction: column;
  }
  ${mq("tabletLg", "min")} {
    flex-direction: row-reverse;
    align-items: flex-start;
    justify-content: space-between;
  }
`

export const ImageContainer = styled("div")`
  position: relative;
  overflow: scroll;
  ${mq("iphoneSE", "min")} {
    width: 100%;
    margin: 1rem 5%;
    height: 500px;
  }
  ${mq("tablet", "min")} {
    height: 768px;
  }

  ${mq("tabletLg", "min")} {
    margin: 0;
    width: 80%;
    height: 100%;
  }
`

export const ImageButton = styled("div")`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  height: 100%;
`

export const BackButton = styled("button")`
  display: flex;
  justify-content: center;
  align-items: center;
  border: 2px solid #002578;
  color: #002578;
  height: 40px;
  transition: all 300ms ease-in-out;
  padding: 0.5rem 1rem;
  overflow: hidden;
  i {
    margin-right: 0.3rem;
  }
  &:hover {
    background-color: #002578;
    color: #fff;
  }

  ${mq("iphoneSE", "min")} {
    font-size: 1.2rem;
    margin: 1.2rem;
  }

  ${mq("tabletLg", "min")} {
    font-size: 1rem;
    margin: 0.2rem;
  }
`
export const SecondaryContainer = styled("div")`
  display: flex;
  ${mq("iphoneSE", "min")} {
    justify-content: flex-start;
    align-items: center;
    flex-direction: column;
    width: 100%;
    padding: 0.5rem;
    margin: 0.5rem 0;
  }
  ${mq("tabletLg", "min")} {
    width: 20%;
  }
`
export const ThumbnailWrapper = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;
  ${mq("tabletLg", "min")} {
    flex-direction: column;
  }
`

export const ThumbnailContainer = styled("div")`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
`

export const Thumbnail = styled("div")`
  position: relative;
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100px;
  height: 100px;
  padding: 0.5rem;
  overflow: hidden;
`

export const ThumbnailButton = styled("button")`
  position: absolute;
  width: 100%;
  height: 100%;
`
