import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"

export const ShopContainer = styled("div")`
  position: relative;
  width: 100%;
  height: 100%;
  z-index: 0;
`

export const ShopGridContainer = styled("div")`
  position: relative;
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  row-gap: 30px;
  width: 100%;
  height: 100%;
  margin-top: 60px;
  ${mq("iphoneSE", "min")} {
    column-gap: 0.5rem;
    padding: 1.2rem;
  }

  ${mq("tablet", "min")} {
    column-gap: 1.5rem;
    padding: 2rem;
  }
  ${mq("tabletLg", "min")} {
    column-gap: 3rem;
  }
`

export const FootContainer = styled("div")`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 1rem;
  color: #002578;
  p {
    i {
      margin: 0 0.5rem;
    }
  }
`
