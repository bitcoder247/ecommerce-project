import { Link } from "gatsby"
import { keyframes } from "@emotion/core"
import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"
import logo from '../../../content/dessert_logo_final_01.svg'

export const ShopNav = styled("nav")`
  position: fixed;
  width: 100%;
  height: 60px;
  top: 0;
  background-color: #002578;
  z-index: 1;
`

export const NavContainer = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;
  margin: 0 auto;
  ${mq("iphoneSE", "min")} {
    width: 100%;
  }
  ${mq("desktop", "min")} {
    width: 100%;
  }
`

export const ShopLogoContainer = styled(Link)`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  padding: 0.3rem;
`

export const Logo = styled(logo)`
  width: 100%;
  height: 100%;
`

export const CartContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 60px;
  height: 60px;
`

const grow = keyframes`
  0% {
    transform: scale(0);
  }
  50% {
    transform: scale(1.5);
  }
  70% {
    transform: scale(1.3);
  }
  100% {
    transform: scale(1);
  }
`

export const ShopButton = styled("button")`
  position: relative;
  width: 100%;
  height: 100%;
  transition: border 200ms solid;
  i {
    font-size: 1.5rem;
    color: rgba(255, 255, 255, 0.4);
    transition: color 200ms ease-in-out;
    animation-name: ${grow};
    animation-duration: 400ms;
    animation-timing-function: ease-in-out;
    animation-iteration-count: 1;
  }
  &:hover {
    border: 1px #f7d30b solid;
    i {
      color: #fff;
    }
  }
`
export const NumberDisplay = styled("div")`
  position: absolute;
  justify-content: center;
  align-items: center;
  position: absolute;
  font-weight: 700;
  color: #002578;
  top: 10%;
  right: 10%;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background-color: #f7d30b;
  transform: scale(0);
  &.display-appear,
  &.display-enter {
    transform: scale(0);
  }
  &.display-appear-active,
  &.display-enter-active {
    transform: scale(1);
    transition: all 300ms ease-in-out;
  }
  &.display-appear-done,
  &.display-enter-done {
    transform: scale(1);
  }
`
