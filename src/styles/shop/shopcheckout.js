import { Link } from "gatsby"
import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"

export const ShopCheckOutContainer = styled("div")`
  position: relative;
  top: 0;
  left: 0;
  height: 100%;
  z-index: 3;
  overflow: visible;
`

export const ShopCheckoutWrapper = styled("div")`
  top: 0;
  right: 0;
  height: 100%;
  background-color: rgba(250, 250, 250, 1);
  transform: translateX(100%);
  z-index: 3;
  overflow: visible;

  &.slide-enter {
    transform: translateX(100%);
  }
  &.slide-enter-active {
    transform: translateX(0);
    transition: transform 400ms ease-in-out;
  }
  &.slide-enter-done {
    transform: translateX(0);
  }

  &.slide-exit {
    transform: translateX(0);
  }
  &.slide-exit-active {
    transform: translateX(100%);
    transition: transform 400ms ease-in-out;
  }

  ${mq("iphoneSE", "min")} {
    width: 100%;
  }
  ${mq("small", "min")} {
    width: 100%;
  }
  ${mq("phoneLg", "min")} {
    width: 100%;
  }
  ${mq("tablet", "min")} {
    width: 100%;
  }
  ${mq("tabletLg", "min")} {
    max-width: 500px;
  }
`

export const CheckoutBody = styled("div")`
  width: 100%;
  height: 88%;
  overflow-y: scroll;

  ${mq("iphoneSE", "min")} {
    padding: 0.5rem;
  }
  ${mq("tabletLg", "min")} {
    padding: 1rem;
  }
`

export const ShadowedCover = styled("div")`
  position: absolute;
  top: 0;
  left: 0;
  width: 100vw;
  height: 100%;
  transform: translate(-100%, 60px);
  background-color: rgba(0, 0, 0, 0.3);
  z-index: -1;
`

export const CheckoutNav = styled("div")`
  display: flex;
  justify-content: space-between;
  alice-items: center;
  width: 100%;
  height: 80px;
  background-color: #fff;
  padding: 0 1rem;
`

export const NavTitle = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100%;
  transform: translateX(50px);
  h2 {
    font-size: 1.1rem;
    color: #002578;
  }
`

export const NumberDisplay = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;

  h6 {
    margin-right: 0.5rem;
    color: #002578;
  }
  div {
    display: flex;
    justify-content: center;
    align-items: center;
    width: 40px;
    height: 40px !important;
    border-radius: 50%;
    background-color: #f7d30b;
    margin-right: 0.5rem;
    color: #002578;
    h3 {
      font-size: 1.2rem;
      font-color: #002578;
    }
  }
`

export const EmptyDisplay = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  padding: 1rem;
  color: #002578;
  i {
    font-size: 5rem;
    margin-bottom: 0.5rem;
  }
  p {
    font-size: 1.2rem;
    margin-bottom: 0.2rem;
  }
`

export const ItemKey = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  border-bottom: 1px dotted #002578;
  overflow: hidden;
  span:nth-of-type(1) {
    flex-grow: 1;
    overflow: hidden;
    color: #002578;
  }
  span:nth-of-type(2),
  span:nth-of-type(3) {
    padding: 0.3rem;
    color: #002578;
    overflow: hidden;
    margin-right: 0.8rem;
  }
`

export const ItemList = styled("ul")`
  width: 100%;
  max-height: 70vh;
  min-height: 30vh;
  overflow-y: scroll;
  ${mq("iphoneSE", "min")} {
  }
`

export const CheckoutItem = styled("li")`
  display: block;
  width: 100%;
  height: 120px;
  overflow: hidden;

  ${mq("iphoneSE", "min")} {
    margin: 0;
    padding: 0;
  }
  ${mq("tabletLg", "min")} {
    padding: 0.5rem;
    margin: 0.5rem;
  }
`

export const ThumbnailContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 60px;
  height: 60px;
`
export const Item = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  height: 100%;
  margin: 0 auto;
  overflow: visible;
`

export const ItemDisplay = styled("div")`
  display: flex;
  justify-content: center;
  align-items: flex-start;
`

export const Description = styled("div")`
  color: #444;
  padding: 0.3rem;
  overflow: hidden;

  span:nth-of-type(1),
  span:nth-of-type(2) {
    display: block;
    margin: 0.3rem;
    font-size: 0.85rem;
  }
`
export const ItemForm = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;
  overflow: hidden;
`

export const QuantityInput = styled("input")`
  height: 60px;
  width: 60px;
  margin: auto 0;
`

export const RemoveButton = styled("button")`
  width: 60px;
  height: 60px;
  margin: auto 0;
  background-color: #fff;
  color: #002578;
  &:hover {
    background-color: #cb0b0b;
    color: #fff;
  }
`
export const CostContainer = styled("div")`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  flex-direction: column;
  width: 100%;
  height: 30vh;
  div {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    width: 100%;
    overflow: hidden;
    span {
      color: #002578;
      margin: 0.3rem 0;
      width: 50%;
      text-align: end;
      overflow: hidden;
    }
    strong {
      overflow: hidden;
    }
  }
  div:nth-of-type(4) {
    padding-top: 0.5rem;
    border-top: 1px dotted #002578;
    strong {
      color: #002578;
    }
  }
`

export const CheckoutButton = styled("button")`
  width: 100%;
  color: #fff;
  background-color: #002578;
  font-size: 1.1rem;
  padding: 0.75rem 1.2rem;
  margin: 1rem auto;
  transition: all 400ms ease-in-out;
  z-index: 5;
  i {
    margin-left: 0.5rem;
  }
  &:hover {
    color: #000;
    background-color: #f7d30b;
  }
`

export const GoBackButton = styled(Link)`
  display: block;
  width: 100%;
  color: #002578;
  border: 2px #002578 solid;
  font-size: 1.1rem;
  width: 100%;
  padding: 0.75rem 1.2rem;
  margin: 1rem auto;
  transition: all 400ms ease-in-out;
  text-align: center;
  i {
    margin-right: 0.5rem;
  }
  &:hover {
    color: #fff;
    background-color: #002578;
  }
`

export const Gift = styled("div")`
  display: flex;
  align-items: center;
  background-color: rgba(0, 37, 120, 0.2);
  width: 70%;
  margin: 0 auto;
  color: #002578;
  padding: 1rem;
  p {
    display: flex;
    justify-content: flex-start;
    font-size: 1rem;
    i {
      font-size: 0.9rem;
      margin-right: 0.5rem;
      margin-top: 0.3rem;
    }
  }

  i {
    font-size: 1.5rem;
    margin-left: 0.5rem;
  }

  ${mq("iphoneSE", "min")} {
    justify-content: center;
    width: 100%;
  }
  ${mq("phoneLg", "min")} {
    width: 70%;
  }
  ${mq("tabletLg", "min")} {
    justify-content: flex-start;
  }
`
export const ShippingInfo = styled("div")`
  display: flex;
  align-items: center;
  background-color: rgba(0, 37, 120, 0.2);
  width: 70%;
  margin: 1rem auto;
  color: #002578;
  padding: 1rem;

  p {
    font-size: 1rem;
    span {
      font-weight: 700;
    }
    i {
      font-size: 0.9rem;
      margin-right: 0.5rem;
      margin-top: 0.3rem;
    }
  }
  ${mq("iphoneSE", "min")} {
    justify-content: center;
    width: 100%;
  }
  ${mq("phoneLg", "min")} {
    width: 70%;
  }
  ${mq("tabletLg", "min")} {
    justify-content: flex-start;
  }
`

export const FootContainer = styled("div")`
  width: 100%;
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 1rem;
  color: #002578;
  p {
    i {
      margin: 0 0.5rem;
    }
  }
`
