import { keyframes } from "@emotion/core"
import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"

export const ButtonContainer = styled("div")`
  justify-content: center;
  align-items: center;
  width: 60px;
  height: 60px;

  position: absolute;
  top: 0;
  left: 0;
  transform: translateX(-100%);
  z-index: 5;
`

const grow = keyframes`
  0% {
    transform: scale(0);
  }
  50% {
    transform: scale(1.5);
  }
  70% {
    transform: scale(1.3);
  }
  100% {
    transform: scale(1);
  }
`

export const ShopButton = styled("button")`
  position: relative;
  width: 100%;
  height: 100%;
  transition: border 200ms solid;
  z-index: 3;
  i {
    font-size: 1.5rem;
    color: rgba(255, 255, 255, 0.4);
    transition: color 200ms ease-in-out;
    animation-name: ${grow};
    animation-duration: 1000ms;
    animation-timing-function: ease-in-out;
    animation-iteration-count: 1;
  }
  &:hover {
    border: 1px #f7d30b solid;
    i {
      color: #fff;
    }
  }
`
export const NumberDisplay = styled("div")`
  position: absolute;
  display: flex;
  justify-content: center;
  align-items: center;
  position: absolute;
  font-weight: 700;
  color: #002578;
  top: 10%;
  right: 10%;
  width: 20px;
  height: 20px;
  border-radius: 50%;
  background-color: #f7d30b;
  transform: scale(0);
  &.display-appear,
  &.display-enter {
    transform: scale(0);
  }
  &.display-appear-active,
  &.display-enter-active {
    transform: scale(1);
    transition: all 300ms ease-in-out;
  }
  &.display-appear-done,
  &.display-enter-done {
    transform: scale(1);
  }
`

export const ExitButtonContainer = styled("div")`
  justify-content: center;
  align-items: center;
  width: 60px;
  height: 60px !important;

  ${mq("iphoneSE", "min")} {
    position: absolute;
    top: 10px;
    left: 0;
    z-index: 5;
  }

  ${mq("tabletLg", "min")} {
    position: absolute;
    top: 0;
    left: 0;
    transform: translateX(-100%);
    z-index: 5;
  }
`

export const ExitButton = styled("button")`
  position: relative;
  width: 100%;
  height: 100%;
  background-color: transparent;
  transition: all 200ms solid;
  i {
    font-size: 1.5rem;
    transition: color 200ms ease-in-out;
    animation-name: ${grow};
    animation-duration: 1000ms;
    animation-timing-function: ease-in-out;
    animation-iteration-count: 1;
  }

  ${mq("iphoneSE", "min")} {
    background-color: transparent;
    i {
      color: #002578;
    }
    &:hover {
      border: 1px #f7d30b solid;
      background-color: #002578;
      i {
        color: #fff;
      }
    }
  }

  ${mq("tabletLg", "min")} {
    i {
      color: rgba(255, 255, 255, 0.4);
    }
    transition: all 200ms solid;
    &:hover {
      border: 1px #f7d30b solid;
      i {
        color: #fff;
      }
    }
  }
`
