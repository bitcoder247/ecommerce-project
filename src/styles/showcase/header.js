import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"

export const Header = styled("header")`
  position: relative;
  background: linear-gradient(45deg, #00216c, #193a85);
  width: 100vw;

  ${mq("iphoneSE", "min")} {
    height: 100vh;
  }

  ${mq("iphoneSE", "min")} and (orientation: landscape) {
    height: 100%;
  }

  ${mq("tablet", "min")} {
    height: 100vh;
  }

  ${mq("tablet", "min")} and (orientation: landscape) {
    height: 100%;
  }

  ${mq("desktop", "min")} {
    height: 100vh;
  }
`
