import { Link } from "gatsby"
import styled from "@emotion/styled"
import { keyframes } from "@emotion/core"
import mq from "../../utils/breakpoints"
import cone from "../../../content/cone_opt_01.svg"

export const ShowcaseCenterOne = styled("div")`
  display: flex;
  justify-content: space-around;
  align-items: flex-start;
  flex-direction: column;
  overflow: hidden;
  grid-column-start: 1;
  grid-column-end: 13;
  grid-row-start: 2;
  grid-row-end: 3;

  ${mq("desktop", "min")} {
    grid-column-start: 1;
    grid-column-end: 13;
    grid-row-start: 2;
    grid-row-end: 3;
  }
`

export const ShowcasePitch = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-direction: column;
  overflow: hidden;

  em {
    font-weight: 700;
    color: #eee;
    overflow: hidden;
  }

  ${mq("iphoneSE", "min")} {
    width: 100%;
    h1 {
      font-size: 1.8rem;
      font-family: "Baloo";
      overflow: hidden;
    }
  }

  ${mq("desktop", "min")} {
    align-self: flex-end;
    width: 50%;
    margin: 0 auto 0 32%;

    h1 {
      font-family: "Baloo";
      overflow: hidden;
      font-size: 2.2rem;
      margin-bottom: 0.2rem;
      color: #eee;
    }
  }
`

export const HorizontalBar = styled("div")`
  display: block;
  width: 30%;
  height: 3px;
  background-color: #eee;
`

export const ShowcaseCenterTwo = styled("div")`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  grid-column-start: 1;
  grid-column-end: 13;
  grid-row-start: 3;
  grid-row-end: 4;
`

/* ------ Icicles & Animation ----- */

const melt = keyframes`
  100% {
    opacity: 0;
    transform: scale(1, 0);
  }
`
const shake = keyframes`
  10%, 90% {
    transform: translateX(2px);
  }
  20%, 60% {
    transform: translate(-2px);
  }
  30%, 50%, 70% {
    transform: translateX(4px);
  }
  40%, 80% {
    transform: translateX(-4px);
  }
`

export const ShowcaseButton = styled("button")`
  position: relative;
  padding: 0.75rem 1.2rem;
  color: #eee;
  transition: all 400ms ease-in;
  font-weight: 700;
  overflow: hidden !important;

  &::before {
    content: "";
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    transform: scale(0);
    transition: all 500ms ease-in-out;
    border-radius: 50%;
    transform-origin: center;
    z-index: -1;
  }
  &:hover::before {
    background-color: #f7d30b;
    transform: scale(2);
  }
  &:hover {
    color: #000;
    animation-name: ${shake};
    animation-duration: 1s;
    animation-timing-function: linear;
    animation-iteration-count: 1;
    background-color: transparent;
  }

  &:hover ~ svg {
    animation-name: ${melt};
    animation-duration: 1s;
    animation-timing-function: ease-in-out;
    animation-iteration-count: 1;
    animation-fill-mode: both;
  }

  ${mq("iphoneSE", "min")} {
    background-color: rgba(123, 183, 229, 0.9);
  }

  ${mq("desktop", "min")} {
    background-color: rgba(123, 183, 229, 0.4);
  }
`
export const SnowSVG = styled("svg")`
  position: absolute;
  left: 0;
  top: 0;
  width: 100%;
  height: 10px;
  overflow: visible;
  transform-origin: top left;
  path {
    fill: rgba(250, 250, 250, 1);
  }
`

export const ShowcaseLink = styled(Link)`
  text-decoration: none;
  width: 100%;
  height: 100%;
  color: #fff;
  transition: all 400ms ease-in;
  &:hover {
    color: #000;
  }
`


export const IcicleSVG = styled("svg")`
  position: absolute;
  left: 0;
  bottom: -30px;
  width: 100%;
  height: 30px;
  cursor: default;
  overflow: visible;
  transform-origin: top left;
  polygon {
    fill: rgba(123, 183, 229, 0.2);
  }
`

/* ---------------------------- */

/* ----------- Cone & Animation ------------ */
const move = keyframes`
  0% {
    transform: translateY(0px); 
  }
  50% {
    transform: translateY(0.3px);
  }
  100% {
    transform: translateY(0px);
  }
`

const moveTwo = keyframes`
  0% {
    transform: translateY(0);
  }
  50% {
    transform: translateY(0.2px);
  }
  100% {
    transform: translateY(0);
  }
`

export const ConeSvg = styled(cone)`
  width: 100%;
  height: 100%;

  #meltOne {
    animation-name: ${move};
    animation-duration: 4s;
    animation-iteration-count: infinite;
    animation-timing-function: ease-in-out;
    animation-delay: 5s;
  }
  #meltTwo {
    animation-name: ${move};
    animation-duration: 4s;
    animation-iteration-count: infinite;
    animation-timing-function: ease-in-out;
    animation-delay: 5s;
  }
  #meltThree {
    animation-name: ${moveTwo};
    animation-duration: 4s;
    animation-iteration-count: infinite;
    animation-timing-function: ease-in-out;
    animation-delay: 5s;
  }
`
/* ----------------------------- */

export const AbsoluteLargeCircle = styled("div")`
  position: absolute;
  border-radius: 50%;
  background: linear-gradient(-135deg, #285bcd, #002578);
  z-index: 0;

  ${mq("iphoneSE", "min")} {
    width: 300px;
    height: 300px;
    bottom: 5%;
    left: -15%;
  }

  ${mq("tablet", "min")} {
    width: 500px;
    height: 500px;
    left: 14%;
    bottom: 5%;
  }

  ${mq("desktop", "min")} {
    width: 500px;
    height: 500px;
    left: 19%;
    bottom: 5%;
  }
`
export const Triangle = styled("div")`
  width: 0;
  height: 0;
  border-bottom: 40px solid transparent;
  border-right: 40px solid #285bcd;
`
