import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"

export const ShowcaseContainer = styled("div")`
  position: relative;
  display: grid;
  grid-template-columns: repeat(12, 1fr);
  grid-template-rows: 100px auto 80px;
  grid-column-start: 1;
  grid-column-end: 13;
  grid-row-start: 1;
  grid-row-end: 3;
  color: #eee;
  overflow: hidden;
  z-index: 1;
`

export const ShowcaseFlexWrapper = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 100%;
  width: 100%;

  ${mq("desktop", "min")} {
    max-width: 1200px;
    margin: 0 auto;
  }
`

export const LogoContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.5rem;
  height: 100%;
`

/* SVG height: 75px width: 42px */
export const SVGContainer = styled("div")`
  position: absolute;
  overflow: hidden;

  ${mq("iphoneSE", "min")} {
    width: 126px;
    height: 225px;
    left: 5%;
    bottom: 1%;
  }
  ${mq("tablet", "min")} {
    width: 252px;
    height: 450px;
    left: 11%;
    bottom: 0;
    padding-bottom: 0.5rem;
  }
  ${mq("desktop", "min")} {
    width: 294px;
    height: 525px;
    left: 13%;
    bottom: 0;
    padding-bottom: 0.5rem;
  }
`
export const DividerContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  overflow: hidden;

  ${mq("iphoneSE", "min")} {
    width: 100%;
    padding: 0.5rem;
  }
  ${mq("desktop", "min")} {
    width: 60%;
    padding: 0.5rem;
  }
`

export const ShowcaseIconWrapper = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  border-radius: 50%;
  background-color: #eee;
  margin: 0 1rem;
  i {
    font-size: 1rem;
    padding: 0.5rem;
    color: #00216c;
  }
`

export const ShowcaseButtonContainer = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;

  ${mq("iphoneSE", "min")} {
    width: 100%;
    overflow: visible;

    ul {
      display: flex;
      justify-content: space-around;
      align-items: center;
      width: 100%;
      overflow: visible;
      li {
        display: flex;
        justify-content: center;
        align-items: center;
        padding: 1rem 0;
        overflow: visible;
        div {
          position: relative;
          overflow: visible;
        }
      }
    }
  }

  ${mq("iphoneSE", "min")} and (orientation: landscape) {
    margin: 1.5rem 0;
  }

  ${mq("desktop", "min")} {
    align-self: flex-end;
    width: 50%;
    margin: 0;
    flex-direction: column;
    button {
      overflow: visible;
    }

    ul {
      display: block;
      overflow: visible;
      width: auto;
      li {
        display: flex;
        justify-content: flex-end;
        align-items: center;
        padding: 1rem 0;
        margin: 1rem 0;
        overflow: visible;
      }
    }
  }
`

export const ShowcaseActionContainer = styled("div")`
  display: flex;
  align-items: center;
  overflow: visible;
  div {
    position: relative;
    overflow: visible;
  }

  ${mq("iphoneSE", "min")} {
    justify-content: center;
    width: 100%;
  }

  ${mq("iphoneSE", "min")} and (orientation: landscape) {
    margin: 1rem 0;
  }

  ${mq("desktop", "min")} {
    justify-content: flex-start;
    align-self: flex-end;
    width: 50%;
    margin: 0;
    padding-bottom: 1rem;
    button {
      overflow: visible;
    }
  }
`

export const ShowcaseFooterContainer = styled("div")`
  display: flex;
  justify-content: space-around;
  align-items: center;
  height: 50%;
  background-color: #285bcd;

  p {
    font-weight: 700;
  }
  button {
    display: flex;
    justify-content: center;
    align-items: center;
  }

  ${mq("iphoneSE", "min")} {
    width: 90%;
    p {
      font-weight: 700;
    }
    button {
      margin: 0 0.3rem;
      i {
        font-size: 1rem;
        color: #eee;
        transition: color 300ms ease-in-out;
      }
      &:hover {
        i {
          color: #f7d30b;
        }
      }
    }
  }

  ${mq("tablet", "min")} {
    width: 50%;
    button {
      margin: 0 1rem;
      i {
        font-size: 1.6rem;
        color: #eee;
        transition: color 300ms ease-in-out;
      }
      &:hover {
        i {
          color: #f7d30b;
        }
      }
    }
  }
`
