import styled from "@emotion/styled"
import mq from "../../utils/breakpoints"
import logo from '../../../content/dessert_logo_final_01.svg'


export const ShowcaseNav = styled("div")`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  grid-column-start: 1;
  grid-column-end: 13;
  grid-row-start: 1;
  grid-row-end: 2;
  width: 100%;
  ${mq("desktop", "min")} {
    
  }
`

export const ShoppingCart = styled("div")`
  display: flex;
  justify-content: center;
  align-items: center;
  padding: 0.3rem;
  height: 100%;

  a {
    color: rgba(255, 255, 255, 0.4);
    font-size: 1.5rem;
    padding: 0.5rem;
  }

  &:hover {
    button {
      color: #fff;
    }
  }
`
export const LogoSVG = styled(logo)`
  width: 100%;
  height: 100%;
`
