import React from "react"

import ProductPages from "../components/product/ProductPages"

const ProductPageTemplate = () => {
  return <ProductPages />
}

export default ProductPageTemplate

// export const query = graphql`
//   query {
//     stripeData: allStripeSku(sort: { fields: [product___name], order: ASC }) {
//       skus: edges {
//         sku: node {
//           id
//           product: product {
//             id
//             name
//             imgobj: localFiles {
//               img: childImageSharp {
//                 fluid {
//                   src
//                 }
//               }
//             }
//           }
//           skuImgObj: localFiles {
//             skuImg: childImageSharp {
//               fluid {
//                 src
//               }
//             }
//           }
//         }
//       }
//     }
//   }
// `
